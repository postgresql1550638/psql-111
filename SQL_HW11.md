// q1
(select first_name from actor)
union
(select first_name from customer)

// q2
(select first_name from actor)
intersect
(select first_name from customer)

// q3
(select first_name from actor)
except
(select first_name from customer)